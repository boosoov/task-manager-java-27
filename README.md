# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: BORUAK SERGEY

**E-MAIL**: boosoov@gmail.com

# HARDWARE

**CPU**: Intel Core i3

**RAM**: 2 GB

**ROM**: 1 GB

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM BUILD 

```bash
mvn clean package
```

# PROGRAM RUN SERVER

```bash
java -jar ./tm-server/target/tm-server.jar
```

# PROGRAM RUN CLIENT

```bash
java -jar ./tm-client/target/tm-client.jar
```

# LINKS TO SKREENSHOTS
    
JSE-27: https://drive.google.com/drive/folders/11u9Uu_CMEd03CCZ9D2BZR4hikcmA64hK?usp=sharing