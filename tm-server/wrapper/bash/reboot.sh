#!/usr/bin/env bash

PORT="8080"
if [ -n "$1" ]; then
  PORT=$1;
fi
FILE_PID="server-$PORT.pid"

echo "REBOOT TASK-MANAGER SERVER $PORT..."

if [ ! -f ./$FILE_PID ]; then
	echo "tm-server pid not found! server is not running!"
	exit 1;
fi

echo "KILL PROCESS WITH PID "$(cat ./$FILE_PID);
kill -9 $(cat ./$FILE_PID)
rm ./$FILE_PID

mkdir -p ../bash/logs
rm -f ../bash/logs/tm-server.log
nohup java -Dport=$PORT -jar ../../tm-server.jar > ../bash/logs/tm-server.log 2>&1 &
echo $! > ./$FILE_PID
echo "TASK-MANAGER SERVER IS RUNNING WITH PID "$!
