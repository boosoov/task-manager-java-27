package com.rencredit.jschool.boruak.taskmanager.unit.service;

import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import com.rencredit.jschool.boruak.taskmanager.locator.ServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.service.AuthService;
import com.rencredit.jschool.boruak.taskmanager.service.PropertyService;
import com.rencredit.jschool.boruak.taskmanager.service.SessionService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import com.rencredit.jschool.boruak.taskmanager.util.EntityManagerFactoryUtil;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class AuthServiceTest {

    @NotNull final EntityManagerFactoryUtil entityManagerFactoryUtil = new EntityManagerFactoryUtil(new PropertyService());
    @NotNull ServiceLocator serviceLocator;
    @NotNull UserService userService;
    @NotNull AuthService authService;
    @NotNull SessionService sessionService;
    @NotNull PropertyService propertyService;

    @Nullable String passwordHash;
    @NotNull User user;
    @NotNull SessionDTO session;

    @Before
    public void init() throws EmptyUserException, EmptyHashLineException, EmptyLoginException, DeniedAccessException, EmptyPasswordException, BusyLoginException, EmptyRoleException {
        serviceLocator = new ServiceLocator();
        userService = (UserService)serviceLocator.getUserService();
        authService = (AuthService)serviceLocator.getAuthService();
        sessionService = (SessionService)serviceLocator.getSessionService();
        propertyService = (PropertyService)serviceLocator.getPropertyService();

        userService.clearAll();
        propertyService.init();
        passwordHash = HashUtil.getHashLine("password");
        user = new User("login", passwordHash);
        serviceLocator.getUserService().addUser("login", user);
        session = sessionService.open("login", "password");
    }

    @Test(expected = EmptySessionException.class)
    public void testNegativeCheckRolesWithoutSession() throws EmptyRoleException, EmptyIdException, NotExistUserException, EmptyUserIdException, EmptySessionException, DeniedAccessException {
        authService.checkRoles(null, new Role[]{Role.USER});
    }

    @Test(expected = EmptyRoleException.class)
    public void testNegativeCheckRolesWithoutRoles() throws EmptyRoleException, EmptyIdException, NotExistUserException, EmptyUserIdException, EmptySessionException, DeniedAccessException {
        authService.checkRoles(session, null);
    }

    @Test(expected = EmptyRoleException.class)
    public void testNegativeCheckRolesWithoutUserId() throws EmptyRoleException, EmptyIdException, NotExistUserException, EmptyUserIdException, EmptySessionException, DeniedAccessException {
        session.setUserId(null);
        authService.checkRoles(session, null);
    }

    @Test(expected = EmptyRoleException.class)
    public void testNegativeCheckRolesWithoutUserInBase() throws EmptyRoleException, EmptyIdException, NotExistUserException, EmptyUserIdException, EmptySessionException, DeniedAccessException {
        session.setUserId("notCorrect");
        authService.checkRoles(session, null);
    }

    @Test(expected = DeniedAccessException.class)
    public void testNegativeCheckRolesWithNotCorrectRole() throws EmptyRoleException, EmptyIdException, NotExistUserException, EmptyUserIdException, EmptySessionException, DeniedAccessException {
        Assert.assertFalse(authService.checkRoles(session, new Role[]{Role.ADMIN}));
    }

    @Test
    public void testCheckRoles() throws EmptyRoleException, EmptyIdException, NotExistUserException, EmptyUserIdException, EmptySessionException, DeniedAccessException {
        Assert.assertTrue(authService.checkRoles(session, new Role[]{Role.USER}));
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeRegistrationLoginPasswordWithoutLogin() throws EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration(null, "password");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeRegistrationLoginPasswordWithoutPassword() throws EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration("login", null);
    }

    @Test
    public void testRegistrationLoginPassword() throws EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration("login2", "password2");
        Assert.assertNotNull(userService.getByLogin("login2"));
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeRegistrationLoginPasswordEmailWithoutLogin() throws EmptyHashLineException, EmptyEmailException, EmptyFirstNameException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration(null, "password", "firstName");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeRegistrationLoginPasswordEmailWithoutPassword() throws EmptyHashLineException, EmptyEmailException, EmptyFirstNameException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration("login", null, "firstName");
    }

    @Test(expected = EmptyEmailException.class)
    public void testNegativeRegistrationLoginPasswordEmailWithoutEmail() throws EmptyHashLineException, EmptyEmailException, EmptyFirstNameException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration("login", "password", (String) null);
    }

    @Test
    public void testRegistrationLoginPasswordEmail() throws EmptyHashLineException, EmptyEmailException, EmptyFirstNameException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration("login2", "password2", "firstName");
        @Nullable final UserDTO user = userService.getByLogin("login2");
        Assert.assertNotNull(user);
        Assert.assertEquals("firstName", user.getFirstName());
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeRegistrationLoginPasswordRoleWithoutLogin() throws EmptyPasswordException, EmptyHashLineException, BusyLoginException, EmptyRoleException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration(null, "password", Role.USER);
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeRegistrationLoginPasswordRoleWithoutPassword() throws EmptyPasswordException, EmptyHashLineException, BusyLoginException, EmptyRoleException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration("login", null, Role.USER);
    }

    @Test(expected = EmptyRoleException.class)
    public void testNegativeRegistrationLoginPasswordRoleWithoutRole() throws EmptyPasswordException, EmptyHashLineException, BusyLoginException, EmptyRoleException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration("login", "password", (Role) null);
    }

    @Test
    public void testRegistrationLoginPasswordRole() throws EmptyPasswordException, EmptyHashLineException, BusyLoginException, EmptyRoleException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration("login2", "password2", Role.USER);
        @Nullable final UserDTO user = userService.getByLogin("login2");
        Assert.assertNotNull(user);
        Assert.assertEquals(Role.USER, user.getRole());
    }

}
