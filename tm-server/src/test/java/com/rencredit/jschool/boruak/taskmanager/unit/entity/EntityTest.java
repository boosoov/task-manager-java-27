package com.rencredit.jschool.boruak.taskmanager.unit.entity;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class EntityTest {

    @Test
    public void projectDTOTest() {
        final ProjectDTO project1 = new ProjectDTO();
        Assert.assertNotNull(project1);
        Assert.assertFalse(project1.getId().isEmpty());
        Assert.assertNull(project1.getName());
        Assert.assertSame("", project1.getDescription());
        Assert.assertNull(project1.getUserId());

        final ProjectDTO project2 = new ProjectDTO("1", "name");
        Assert.assertNotNull(project2);
        Assert.assertFalse(project2.getId().isEmpty());
        Assert.assertSame("name", project2.getName());
        Assert.assertSame("", project2.getDescription());
        Assert.assertSame("1", project2.getUserId());

        final ProjectDTO project3 = new ProjectDTO("1", "name", "description");
        Assert.assertNotNull(project3);
        Assert.assertFalse(project3.getId().isEmpty());
        Assert.assertSame("name", project3.getName());
        Assert.assertSame("description", project3.getDescription());
        Assert.assertSame("1", project3.getUserId());
    }

    @Test
    public void taskDTOTest() {
        final TaskDTO task1 = new TaskDTO();
        Assert.assertNotNull(task1);
        Assert.assertFalse(task1.getId().isEmpty());
        Assert.assertNull(task1.getName());
        Assert.assertSame("", task1.getDescription());
        Assert.assertNull(task1.getUserId());

        final TaskDTO task2 = new TaskDTO("1", "name");
        Assert.assertNotNull(task2);
        Assert.assertFalse(task2.getId().isEmpty());
        Assert.assertSame("name", task2.getName());
        Assert.assertSame("", task2.getDescription());
        Assert.assertSame("1", task2.getUserId());

        final TaskDTO task3 = new TaskDTO("1", "name", "description");
        Assert.assertNotNull(task3);
        Assert.assertFalse(task3.getId().isEmpty());
        Assert.assertSame("name", task3.getName());
        Assert.assertSame("description", task3.getDescription());
        Assert.assertSame("1", task3.getUserId());
    }

    @Test
    public void userDTOTest() {
        final UserDTO user1 = new UserDTO();
        Assert.assertNotNull(user1);
        Assert.assertFalse(user1.getId().isEmpty());
        Assert.assertNull(user1.getLogin());
        Assert.assertNull(user1.getPasswordHash());
        Assert.assertNull(user1.getEmail());
        Assert.assertNull(user1.getFirstName());
        Assert.assertNull(user1.getLastName());
        Assert.assertNull(user1.getMiddleName());
        Assert.assertSame(Role.USER, user1.getRole());
        Assert.assertFalse(user1.isLocked());

        final UserDTO user2 = new UserDTO("login", "passwordHast");
        Assert.assertNotNull(user2);
        Assert.assertFalse(user2.getId().isEmpty());
        Assert.assertSame("login", user2.getLogin());
        Assert.assertSame("passwordHast", user2.getPasswordHash());
        Assert.assertNull(user2.getEmail());
        Assert.assertNull(user2.getFirstName());
        Assert.assertNull(user2.getLastName());
        Assert.assertNull(user2.getMiddleName());
        Assert.assertSame(Role.USER, user2.getRole());
        Assert.assertFalse(user2.isLocked());

        final UserDTO user3 = new UserDTO("login", "passwordHast", Role.ADMIN);
        Assert.assertNotNull(user3);
        Assert.assertFalse(user3.getId().isEmpty());
        Assert.assertSame("login", user3.getLogin());
        Assert.assertSame("passwordHast", user3.getPasswordHash());
        Assert.assertNull(user3.getEmail());
        Assert.assertNull(user3.getFirstName());
        Assert.assertNull(user3.getLastName());
        Assert.assertNull(user3.getMiddleName());
        Assert.assertSame(Role.ADMIN, user3.getRole());
        Assert.assertFalse(user3.isLocked());

        final UserDTO user4 = new UserDTO("login", "passwordHast", "firstName");
        Assert.assertNotNull(user4);
        Assert.assertFalse(user4.getId().isEmpty());
        Assert.assertSame("login", user4.getLogin());
        Assert.assertSame("passwordHast", user4.getPasswordHash());
        Assert.assertNull(user4.getEmail());
        Assert.assertSame("firstName", user4.getFirstName());
        Assert.assertNull(user4.getLastName());
        Assert.assertNull(user4.getMiddleName());
        Assert.assertSame(Role.USER, user4.getRole());
        Assert.assertFalse(user4.isLocked());
    }

    @Test
    public void sessionDTOTest() {
        final SessionDTO session1 = new SessionDTO();
        Assert.assertNotNull(session1);
        session1.setTimestamp(1234567L);
        session1.setUserId("1");
        session1.setSignature("signature");
        Assert.assertEquals((Long)1234567L, session1.getTimestamp());
        Assert.assertSame("1", session1.getUserId());
        Assert.assertSame("signature", session1.getSignature());

        final SessionDTO session2 = new SessionDTO(90L,"userId", "signature");
        Assert.assertNotNull(session2);
        Assert.assertEquals((Long)90L, session2.getTimestamp());
        Assert.assertSame("userId", session2.getUserId());
        Assert.assertSame("signature", session2.getSignature());

        final SessionDTO sessionClone = session2.clone();
        Assert.assertNotNull(sessionClone);
        Assert.assertFalse(sessionClone.getId().isEmpty());
        Assert.assertEquals((Long)90L, sessionClone.getTimestamp());
        Assert.assertSame("userId", sessionClone.getUserId());
        Assert.assertSame("signature", sessionClone.getSignature());
    }

}
