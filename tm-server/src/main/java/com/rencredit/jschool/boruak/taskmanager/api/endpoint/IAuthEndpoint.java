package com.rencredit.jschool.boruak.taskmanager.api.endpoint;

import com.rencredit.jschool.boruak.taskmanager.dto.status.Result;
import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAuthEndpoint {

    @NotNull
    @WebMethod
    SessionDTO logIn(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) throws EmptyUserException, EmptyHashLineException, EmptyLoginException, DeniedAccessException, EmptyPasswordException;

    @NotNull
    @WebMethod
    Result logOut(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException;

    @Nullable
    @WebMethod
    String getUserId(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException;

    @WebMethod
    void checkRoles(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "roles", partName = "roles") final Role[] roles
    ) throws DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    void registrationLoginPassword(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) throws EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException;

    @WebMethod
    void registrationLoginPasswordFirstName(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email") final String email
    ) throws EmptyHashLineException, EmptyEmailException, EmptyFirstNameException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException;

    @WebMethod
    void registrationLoginPasswordRole(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "role", partName = "role") final Role role
    ) throws EmptyPasswordException, EmptyHashLineException, BusyLoginException, EmptyRoleException, EmptyLoginException, EmptyUserException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptySessionException;

}
