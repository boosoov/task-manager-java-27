package com.rencredit.jschool.boruak.taskmanager.util;

import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @NotNull final String line = nextLine();
        return Integer.parseInt(line);
    }

}
