package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ICommandRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.ICommandService;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyCommandException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyNameException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    public Map<String, AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @NotNull
    @Override
    public String[] getCommands() {
        @NotNull final String[] commands = commandRepository.getCommands();
        return commands;
    }

    @NotNull
    public String[] getArgs() {
        return commandRepository.getArgs();
    }

    public void putCommand(@Nullable final String name, @Nullable final AbstractCommand command) throws EmptyCommandException, EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (command == null) throw new EmptyCommandException();
        commandRepository.putCommand(name, command);
    }

}
