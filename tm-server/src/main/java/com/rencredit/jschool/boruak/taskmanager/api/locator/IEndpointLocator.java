package com.rencredit.jschool.boruak.taskmanager.api.locator;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.*;
import org.jetbrains.annotations.NotNull;

public interface IEndpointLocator {

    @NotNull
    IServiceLocator getServiceLocator();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    ISessionEndpoint getSessionEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    IAdminEndpoint getAdminEndpoint();

    @NotNull
    IAdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

}
