package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IRepository<TaskDTO> {

    List<TaskDTO> getListDTO();

    @NotNull
    List<Task> getListEntity();

    @NotNull
    List<TaskDTO> findAllByUserIdDTO(@NotNull String userId);

    @NotNull
    List<Task> findAllByUserIdEntity(@NotNull final String userId);

    @NotNull
    List<TaskDTO> findAllDTOByProjectId(@NotNull final String projectId);

    void clearByUserId(@NotNull String userId);

    @Nullable
    TaskDTO findOneDTOById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findOneEntityById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    TaskDTO findOneDTOByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task findOneEntityByIndex(@NotNull final String userId, @NotNull final Integer index);

    @Nullable
    TaskDTO findOneDTOByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task findOneEntityByName(@NotNull final String userId, @NotNull final String name);

    @Nullable
    void removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    void removeOneByName(@NotNull String userId, @NotNull String name);

    void clearAll();

}
