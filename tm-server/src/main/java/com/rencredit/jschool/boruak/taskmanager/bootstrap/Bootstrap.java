package com.rencredit.jschool.boruak.taskmanager.bootstrap;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IEndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.ICommandService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IPropertyService;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownCommandException;
import com.rencredit.jschool.boruak.taskmanager.locator.EndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.locator.ServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.service.PropertyService;
import com.rencredit.jschool.boruak.taskmanager.util.EntityManagerFactoryUtil;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import javax.xml.ws.Endpoint;
import java.lang.reflect.Modifier;
import java.util.Set;

public class Bootstrap {

    @NotNull private static String host;
    
    @NotNull private static Integer port;

    @NotNull
    final IServiceLocator serviceLocator = new ServiceLocator();

    @NotNull
    final IEndpointLocator endpointLocator = new EndpointLocator(serviceLocator);

    @NotNull
    final EntityManagerFactoryUtil entityManagerFactoryUtil = new EntityManagerFactoryUtil(new PropertyService());

    @NotNull
    private final static String pathCommands = "com.rencredit.jschool.boruak.taskmanager.command";

    public Bootstrap() {
    }

    public void run(@Nullable final String[] args){
        try {
            init();
        } catch (DeniedAccessException e) {
            e.printStackTrace();
        } catch (EmptyUserException e) {
            e.printStackTrace();
        } catch (EmptyLoginException e) {
            e.printStackTrace();
        } catch (EmptyHashLineException e) {
            e.printStackTrace();
        } catch (EmptyPasswordException e) {
            e.printStackTrace();
        } catch (BusyLoginException e) {
            e.printStackTrace();
        } catch (EmptyRoleException e) {
            e.printStackTrace();
        } catch (EmptyElementsException e) {
            e.printStackTrace();
        }
        System.out.println("** SERVER IS RUNNING ** \n");
    }

    @SneakyThrows
    public void registryCommand() {
        @NotNull final Reflections reflections = new Reflections(pathCommands);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            @NotNull final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    private void registry(@Nullable final AbstractCommand command) throws EmptyCommandException, EmptyNameException {
        if (command == null) return;
        command.setEndpointLocator(endpointLocator);
        command.setServiceLocator(serviceLocator);
        @NotNull final ICommandService commandService = serviceLocator.getCommandService();
        commandService.putCommand(command.name(), command);
    }

    public void parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        for (@Nullable final String arg : args) {
            processArg(arg);
            System.out.println();
        }
    }

    @SneakyThrows
    private void processArg(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @NotNull final ICommandService commandService = serviceLocator.getCommandService();
        @Nullable final AbstractCommand command = commandService.getTerminalCommands().get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    public void init() throws DeniedAccessException, EmptyRoleException, EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, EmptyElementsException {
        initProperty();
        initEndpoint();
        initUsers();
        closeAllSessions();
    }

    private void initProperty() {
        serviceLocator.getPropertyService().init();
    }

    private void initUsers() throws DeniedAccessException, EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, EmptyElementsException {
        try {
            serviceLocator.getUserService().addUser("1", "1");
        }catch (Exception e){}
        try {
            serviceLocator.getUserService().addUser("test", "test");
        }catch (Exception e){}
        try {
            serviceLocator.getUserService().addUser("admin", "admin", Role.ADMIN);
        }catch (Exception e){}
    }

    private void closeAllSessions()  {
        serviceLocator.getSessionService().closeAll();
    }

    private void initEndpoint() {
        registryEndpoint(endpointLocator.getUserEndpoint());
        registryEndpoint(endpointLocator.getSessionEndpoint());
        registryEndpoint(endpointLocator.getTaskEndpoint());
        registryEndpoint(endpointLocator.getProjectEndpoint());
        registryEndpoint(endpointLocator.getAdminEndpoint());
        registryEndpoint(endpointLocator.getAdminUserEndpoint());
        registryEndpoint(endpointLocator.getAuthEndpoint());
    }

    private void registryEndpoint(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        host = propertyService.getServerHost();
        port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @NotNull
    public IEndpointLocator getEndpointLocator() {
        return endpointLocator;
    }

    public static String getHost() {
        return host;
    }

    public static Integer getPort() {
        return port;
    }
}
