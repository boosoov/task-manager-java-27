package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.IService;
import com.rencredit.jschool.boruak.taskmanager.dto.AbstractEntityDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistAbstractListException;
import com.rencredit.jschool.boruak.taskmanager.repository.AbstractRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public class AbstractService<E extends AbstractEntityDTO> implements IService<E> {

    @Override
    public void load(@Nullable final Collection<E> elements) throws EmptyElementsException {
        if (elements == null || elements.isEmpty()) throw new EmptyElementsException();

        @NotNull final IRepository<E> repository = new AbstractRepository<>();
        try {
            repository.begin();
            repository.load(elements);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void load(@Nullable final E... elements) throws EmptyElementsException {
        if (elements == null || elements.length == 0) throw new EmptyElementsException();

        @NotNull final IRepository<E> repository = new AbstractRepository<>();
        try {
            repository.begin();
            repository.load(elements);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public boolean merge(@Nullable final E element) throws EmptyElementsException {
        if (element == null) throw new EmptyElementsException();

        @NotNull final IRepository<E> repository = new AbstractRepository<>();
        try {
            repository.begin();
            repository.merge(element);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }

        return true;
    }

    @Override
    public void merge(@Nullable final Collection<E> elements) throws NotExistAbstractListException {
        if (elements == null) throw new NotExistAbstractListException();

        @NotNull final IRepository<E> repository = new AbstractRepository<>();
        try {
            repository.begin();
            repository.merge(elements);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void merge(@Nullable final E... elements) throws NotExistAbstractListException {
        if (elements == null || elements.length == 0) throw new NotExistAbstractListException();

        @NotNull final IRepository<E> repository = new AbstractRepository<>();
        try {
            repository.begin();
            repository.merge(elements);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void clearAll() throws EmptyPasswordException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, EmptyRoleException {

        @NotNull final IRepository<E> repository = new AbstractRepository<>();
        try {
            repository.begin();
            repository.clearAll();
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }

    }

}
