package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.repository.ISessionRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.IPropertyService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ISessionService;
import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.repository.SessionRepository;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;
import com.rencredit.jschool.boruak.taskmanager.util.SignatureUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class SessionService implements ISessionService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SessionService(
            @NotNull final IServiceLocator serviceLocator
    ) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    public SessionDTO open(@Nullable final String login, @Nullable final String password) throws EmptyPasswordException, EmptyLoginException, EmptyHashLineException, DeniedAccessException, EmptyUserException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        final boolean check = checkUserAccess(login, password);
        if (!check) throw new DeniedAccessException();
        final UserDTO user = serviceLocator.getUserService().getByLogin(login);
        if (user == null) throw new EmptyUserException();
        if (user.isLocked()) throw new DeniedAccessException();
        final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());

        @Nullable final SessionDTO sessionSign = sign(session);

        @NotNull final ISessionRepository repository = new SessionRepository();
        try {
            repository.begin();
            repository.merge(session);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }

        return sessionSign;
    }

    @Override
    public void close(@Nullable final SessionDTO session) throws DeniedAccessException {
        validate(session);

        @NotNull final ISessionRepository repository = new SessionRepository();
        try {
            repository.begin();
            @Nullable final Session sessionEntity = repository.findById(session.getId());
            if (sessionEntity != null) repository.removeBySession(sessionEntity);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void closeAll(@Nullable final SessionDTO session) throws DeniedAccessException {
        validate(session);

        @NotNull final ISessionRepository repository = new SessionRepository();
        try {
            repository.begin();
            repository.clearAll();
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void closeAll() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        try {
            repository.begin();
            repository.clearAll();
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public UserDTO getUser(@Nullable final SessionDTO session) throws DeniedAccessException, EmptyIdException {
        @Nullable final String userId = getUserId(session);
        return serviceLocator.getUserService().getById(userId);
    }

    @Nullable
    @Override
    public String getUserId(@Nullable final SessionDTO session) throws DeniedAccessException {
        validate(session);
        return session.getUserId();
    }

    @NotNull
    @Override
    public List<SessionDTO> getListSession(@Nullable final SessionDTO session) throws DeniedAccessException {
        validate(session);

        @NotNull final ISessionRepository repository = new SessionRepository();
        List<SessionDTO> list = repository.getListDTO();
        return list;
    }

    @Nullable
    @Override
    public SessionDTO sign(@Nullable final SessionDTO session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean isValid(@Nullable final SessionDTO session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final SessionDTO session) throws DeniedAccessException {
        if (session == null) throw new DeniedAccessException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new DeniedAccessException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new DeniedAccessException();
        if (session.getTimestamp() == null) throw new DeniedAccessException();
        @Nullable final SessionDTO temp = session.clone();
        if (temp == null) throw new DeniedAccessException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new DeniedAccessException();

        @NotNull final ISessionRepository repository = new SessionRepository();
        if (!repository.contains(session.getId())) throw new DeniedAccessException();
    }


    @Override
    public void validate(@Nullable final SessionDTO session, @Nullable final Role role) throws DeniedAccessException, EmptyIdException {
        if (role == null) throw new DeniedAccessException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user = serviceLocator.getUserService().getById(userId);
        if (user == null) throw new DeniedAccessException();
        if (!role.equals((user.getRole()))) throw new DeniedAccessException();
    }

    @Override
    public boolean checkUserAccess(@NotNull final String login, @NotNull final String password) throws EmptyHashLineException, EmptyLoginException {
        @Nullable final UserDTO user = serviceLocator.getUserService().getByLogin(login);
        if (user == null) return false;
        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public void clearAll() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        try {
            repository.begin();
            repository.clearAll();
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

}
