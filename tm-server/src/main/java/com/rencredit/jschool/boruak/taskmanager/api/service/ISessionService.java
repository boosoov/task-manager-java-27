package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionService {

    void close(@Nullable final SessionDTO session) throws DeniedAccessException;

    void closeAll(@Nullable final SessionDTO session) throws DeniedAccessException;

    void closeAll();

    @Nullable
    UserDTO getUser(@Nullable final SessionDTO session) throws DeniedAccessException, EmptyIdException;

    @Nullable
    String getUserId(@Nullable final SessionDTO session) throws DeniedAccessException;

    @NotNull
    List<SessionDTO> getListSession(@Nullable final SessionDTO session) throws DeniedAccessException;

    @Nullable
    SessionDTO sign(@Nullable final SessionDTO session);

    boolean isValid(@Nullable final SessionDTO session);

    void validate(@Nullable final SessionDTO session) throws DeniedAccessException;

    void validate(@Nullable final SessionDTO session, @Nullable final Role role) throws DeniedAccessException, EmptyIdException;

    @Nullable
    SessionDTO open(@Nullable final String login, @Nullable final String password) throws DeniedAccessException, EmptyPasswordException, EmptyLoginException, EmptyHashLineException, EmptyUserException;

    boolean checkUserAccess(@NotNull final String login, @NotNull final String password) throws EmptyHashLineException, EmptyLoginException;

    void clearAll();

}
