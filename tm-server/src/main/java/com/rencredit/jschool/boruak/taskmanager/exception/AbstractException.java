package com.rencredit.jschool.boruak.taskmanager.exception;

public class AbstractException extends Exception {

    public AbstractException() {}

    public AbstractException(final String message) {
        super(message);
    }

    public AbstractException(final Throwable cause) {
        super(cause);
    }

}
