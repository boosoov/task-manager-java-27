package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ITaskRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.ITaskService;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import com.rencredit.jschool.boruak.taskmanager.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class TaskService extends AbstractService<TaskDTO> implements ITaskService {

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final TaskDTO task = new TaskDTO(userId, name);

        @NotNull final ITaskRepository repository = new TaskRepository();
        try {
            repository.begin();
            repository.add(task);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @NotNull final TaskDTO task = new TaskDTO(userId, name, description);

        @NotNull final ITaskRepository repository = new TaskRepository();
        try {
            repository.begin();
            repository.add(task);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final TaskDTO task) throws EmptyTaskException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new EmptyTaskException();

        @NotNull final ITaskRepository repository = new TaskRepository();
        try {
            repository.begin();
            repository.add(task);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final TaskDTO task) throws EmptyTaskException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new EmptyTaskException();

        @NotNull final ITaskRepository repository = new TaskRepository();
        try {
            repository.begin();
            repository.remove(task);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final ITaskRepository repository = new TaskRepository();
        @NotNull final List<TaskDTO> tasks = repository.findAllByUserIdDTO(userId);
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String projectId) throws EmptyProjectIdException {
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();

        @NotNull final ITaskRepository repository = new TaskRepository();
        @NotNull final List<TaskDTO> tasks = repository.findAllDTOByProjectId(projectId);
        return tasks;
    }

    @Override
    public void clearByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final ITaskRepository repository = new TaskRepository();
        try {
            repository.begin();
            repository.clearByUserId(userId);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws EmptyUserIdException, IncorrectIndexException {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final ITaskRepository repository = new TaskRepository();
        @NotNull final TaskDTO task = repository.findOneDTOByIndex(userId, index);
        return task;
    }

    @Nullable
    @Override
    public TaskDTO findOneByName(@Nullable final String userId, @Nullable final String name) throws EmptyUserIdException, EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final ITaskRepository repository = new TaskRepository();
        @Nullable final TaskDTO task = repository.findOneDTOByName(userId, name);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, EmptyDescriptionException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new EmptyTaskException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);

        @NotNull final ITaskRepository repository = new TaskRepository();
        try {
            repository.begin();
            repository.merge(task);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }

        return task;
    }

    @Override
    public void attachTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyTaskIdException, EmptyProjectIdException {
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();

        @Nullable final TaskDTO task = findOneById(userId, taskId);
        if (task == null) throw new EmptyTaskException();
        task.setProjectId(projectId);

        @NotNull final ITaskRepository repository = new TaskRepository();
        try {
            repository.begin();
            repository.merge(task);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void detachTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyTaskIdException, EmptyProjectIdException {
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();

        @Nullable final TaskDTO task = findOneById(userId, taskId);
        if (task == null) throw new EmptyTaskException();
        task.setProjectId(null);

        @NotNull final ITaskRepository repository = new TaskRepository();
        try {
            repository.begin();
            repository.merge(task);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws EmptyTaskException, EmptyNameException, EmptyUserIdException, IncorrectIndexException, EmptyDescriptionException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new EmptyTaskException();
        task.setName(name);
        task.setDescription(description);

        @NotNull final ITaskRepository repository = new TaskRepository();
        try {
            repository.begin();
            repository.merge(task);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }

        return task;
    }

    @Override
    public void removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws IncorrectIndexException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();

        @NotNull final ITaskRepository repository = new TaskRepository();
        try {
            repository.begin();
            repository.removeOneByIndex(userId, index);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final ITaskRepository repository = new TaskRepository();
        try {
            repository.begin();
            repository.removeOneByName(userId, name);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final ITaskRepository repository = new TaskRepository();
        @Nullable final TaskDTO task = repository.findOneDTOById(userId, id);
        return task;
    }

    @Override
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final ITaskRepository repository = new TaskRepository();
        try {
            repository.begin();
            repository.removeOneById(userId, id);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> getList() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        @NotNull final List<TaskDTO> tasks = repository.getListDTO();
        return tasks;
    }

    @Override
    public void clearAll() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        try {
            repository.begin();
            repository.clearAll();
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

}
