package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ITaskRepository;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import com.rencredit.jschool.boruak.taskmanager.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskService extends IService<TaskDTO> {

    void create(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    void create(@Nullable String userId, @Nullable String name, @Nullable String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException;

    void create(@Nullable String userId, @Nullable TaskDTO task) throws EmptyTaskException, EmptyUserIdException;

    void remove(@Nullable String userId, @Nullable TaskDTO task) throws EmptyTaskException, EmptyUserIdException;

    @NotNull
    List<TaskDTO> findAllByUserId(@Nullable String userId) throws EmptyUserIdException;

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable final String projectId) throws EmptyProjectIdException;

    void clearByUserId(@Nullable String userId) throws EmptyUserIdException;

    @Nullable
    TaskDTO findOneById(@Nullable String userId, @Nullable String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    TaskDTO findOneByIndex(@Nullable String userId, @Nullable Integer index) throws EmptyUserIdException, IncorrectIndexException;

    @Nullable
    TaskDTO findOneByName(@Nullable String userId, @Nullable String name) throws EmptyUserIdException, EmptyNameException;

    @Nullable
    void removeOneById(@Nullable String userId, @Nullable String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    void removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws IncorrectIndexException, EmptyUserIdException;

    @Nullable
    void removeOneByName(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    @NotNull
    TaskDTO updateTaskById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, EmptyDescriptionException;

    void attachTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyTaskIdException, EmptyProjectIdException;

    void detachTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyTaskIdException, EmptyProjectIdException;

    @NotNull
    TaskDTO updateTaskByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws EmptyTaskException, EmptyNameException, EmptyUserIdException, IncorrectIndexException, EmptyDescriptionException;

    @NotNull List<TaskDTO> getList();

    void clearAll();

}
