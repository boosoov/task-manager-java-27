package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean checkRoles(@Nullable final SessionDTO session, @Nullable final Role[] roles) throws DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        if (session == null) throw new EmptySessionException();
        if (roles == null || roles.length == 0) throw new EmptyRoleException();
        @Nullable final String userId = session.getUserId();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final UserDTO user = userService.getById(userId);
        if (user == null) throw new NotExistUserException();
        @Nullable final Role role = user.getRole();
        for (final Role item : roles) if (role.equals(item)) return true;
        throw new DeniedAccessException();
    }

    @Override
    public void registration(@Nullable final String login, @Nullable final String password) throws EmptyPasswordException, EmptyLoginException, EmptyUserException, BusyLoginException, DeniedAccessException, EmptyHashLineException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        userService.add(login, password);
    }

    @Override
    public void registration(@Nullable final String login, @Nullable final String password, @Nullable final String firstName) throws EmptyPasswordException, EmptyFirstNameException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, EmptyEmailException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyEmailException();

        userService.add(login, password, firstName);
    }

    @Override
    public void registration(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws EmptyRoleException, EmptyPasswordException, EmptyLoginException, EmptyUserException, BusyLoginException, DeniedAccessException, EmptyHashLineException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();

        userService.add(login, password, role);
    }

}
