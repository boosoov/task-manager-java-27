package com.rencredit.jschool.boruak.taskmanager.api.endpoint;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    void createProjectName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException;

    @WebMethod
    void createProjectNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException, DeniedAccessException;

    @NotNull
    @WebMethod
    List<ProjectDTO> findAllProjectByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws EmptyUserIdException, DeniedAccessException;

    @Nullable
    @WebMethod
    ProjectDTO findProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException;

    @Nullable
    @WebMethod
    ProjectDTO findProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) throws EmptyUserIdException, IncorrectIndexException, DeniedAccessException;

    @Nullable
    @WebMethod
    ProjectDTO findProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException;

    @NotNull
    @WebMethod
    ProjectDTO updateProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) throws EmptyIdException, EmptyNameException, EmptyUserIdException, EmptyProjectException, DeniedAccessException, EmptyDescriptionException;

    @NotNull
    @WebMethod
    ProjectDTO updateProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) throws IncorrectIndexException, EmptyNameException, EmptyUserIdException, EmptyProjectException, DeniedAccessException, EmptyDescriptionException;

    @Nullable
    @WebMethod
    void removeProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException;

    @Nullable
    @WebMethod
    void removeProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) throws EmptyUserIdException, IncorrectIndexException, DeniedAccessException;

    @Nullable
    @WebMethod
    void removeProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException;

    @WebMethod
    void clearAllUserProjects(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws EmptyUserIdException, DeniedAccessException;

    @WebMethod
    void clearAllProject(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException, EmptyUserException, EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyRoleException;

    @NotNull
    @WebMethod
    List<ProjectDTO> getListProject(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException;

    @NotNull
    @WebMethod
    List<TaskDTO> getTasksByProject(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") final String projectId
    ) throws DeniedAccessException, EmptyProjectIdException;

    @WebMethod
    void attachTaskToProject(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") final String projectId,
            @Nullable @WebParam(name = "taskId", partName = "taskId") final String taskId
    ) throws DeniedAccessException, EmptyTaskIdException, EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyProjectIdException;

    @WebMethod
    void detachTaskFromProject(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") final String projectId,
            @Nullable @WebParam(name = "taskId", partName = "taskId") final String taskId
    ) throws DeniedAccessException, EmptyTaskIdException, EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyProjectIdException;

}
