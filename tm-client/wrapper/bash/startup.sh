#!/usr/bin/env bash

echo 'STARTING CLIENT'
if [ -f ./tm-client.pid ]; then
	echo "Task-manager client already started"
	exit 1;
fi

java -jar ../../tm-client.jar
echo $! > ./tm-client.pid
echo "TASK-MANAGER CLIENT IS RUNNING WITH PID "$!
