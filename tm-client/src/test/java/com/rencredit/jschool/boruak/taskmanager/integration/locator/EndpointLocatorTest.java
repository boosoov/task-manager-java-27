package com.rencredit.jschool.boruak.taskmanager.integration.locator;

import com.rencredit.jschool.boruak.taskmanager.locator.EndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(IntegrationWithServerTestCategory.class)
public class EndpointLocatorTest {

    @Test
    public void endpointLocatorTest() {
        @NotNull final EndpointLocator endpointLocator = new EndpointLocator();

        Assert.assertNotNull(endpointLocator.getUserEndpoint());
        Assert.assertNotNull(endpointLocator.getSessionEndpoint());
        Assert.assertNotNull(endpointLocator.getTaskEndpoint());
        Assert.assertNotNull(endpointLocator.getProjectEndpoint());
        Assert.assertNotNull(endpointLocator.getAdminEndpoint());
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint());
        Assert.assertNotNull(endpointLocator.getAuthEndpoint());
    }

}
