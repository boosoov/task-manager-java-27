package com.rencredit.jschool.boruak.taskmanager.integration.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IEndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.locator.EndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

@Category(IntegrationWithServerTestCategory.class)
public class UserEndpointTest {

    @NotNull IEndpointLocator endpointLocator;
    @NotNull UserEndpoint userEndpoint;
    @NotNull AuthEndpoint authEndpoint;
    @NotNull SessionEndpoint sessionEndpoint;
    @NotNull SessionDTO session;

    @Before
    public void init() throws EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, EmptyHashLineException_Exception, DeniedAccessException_Exception, EmptyRoleException_Exception, BusyLoginException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
        endpointLocator = new EndpointLocator();
        userEndpoint = endpointLocator.getUserEndpoint();
        sessionEndpoint = endpointLocator.getSessionEndpoint();
        authEndpoint = endpointLocator.getAuthEndpoint();

        session = sessionEndpoint.openSession("admin", "admin");
    }

    @After
    public void clearAll() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, BusyLoginException_Exception, EmptySessionException_Exception, EmptyLoginException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, EmptyPasswordException_Exception, NotExistUserException_Exception, EmptyRoleException_Exception {
        userEndpoint.clearAllUser(session);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeAddUserLoginPasswordWithoutSession() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPassword(null, "user01", "password");
    }

    @Test(expected = EmptyLoginException_Exception.class)
    public void testNegativeAddUserLoginPasswordWithoutLogin() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPassword(session, null, "password");
    }

    @Test(expected = EmptyPasswordException_Exception.class)
    public void testNegativeAddUserLoginPasswordWithoutPassword() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPassword(session, "user01", null);
    }

    @Test
    public void testAddUserLoginPassword() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPassword(session, "user01", "password");
        @NotNull UserDTO user = userEndpoint.getUserByLogin(session, "user01");
        Assert.assertNotNull(user);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeAddUserLoginPasswordFirstNameWithoutSession() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPasswordFirstName(null, "user01", "password", "FirstName");
    }

    @Test(expected = EmptyLoginException_Exception.class)
    public void testNegativeAddUserLoginPasswordFirstNameWithoutLogin() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPasswordFirstName(session, null, "password", "FirstName");
    }

    @Test(expected = EmptyPasswordException_Exception.class)
    public void testNegativeAddUserLoginPasswordFirstNameWithoutPassword() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPasswordFirstName(session, "user01", null, "FirstName");
    }

    @Test(expected = EmptyFirstNameException_Exception.class)
    public void testNegativeAddUserLoginPasswordFirstNameWithoutFirstName() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPasswordFirstName(session, "user01", "password", null);
    }

    @Test
    public void testAddUserLoginPasswordFirstName() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPasswordFirstName(session, "user01", "password", "FirstName");
        @NotNull UserDTO user = userEndpoint.getUserByLogin(session, "user01");
        Assert.assertNotNull(user);
        Assert.assertEquals("FirstName", user.getFirstName());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeAddUserLoginPasswordRoleWithoutSession() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
        userEndpoint.addUserLoginPasswordRole(null, "user01", "password", Role.USER);
    }

    @Test(expected = EmptyLoginException_Exception.class)
    public void testNegativeAddUserLoginPasswordRoleWithoutLogin() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
        userEndpoint.addUserLoginPasswordRole(session, null, "password", Role.USER);
    }

    @Test(expected = EmptyPasswordException_Exception.class)
    public void testNegativeAddUserLoginPasswordRoleWithoutPassword() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
        userEndpoint.addUserLoginPasswordRole(session, "user01", null, Role.USER);
    }

    @Test(expected = EmptyRoleException_Exception.class)
    public void testNegativeAddUserLoginPasswordRoleWithoutRole() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
        userEndpoint.addUserLoginPasswordRole(session, "user01", "password", null);
    }

    @Test
    public void testAddUserLoginPasswordRole() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
        userEndpoint.addUserLoginPasswordRole(session, "user01", "password", Role.USER);
        @NotNull UserDTO user = userEndpoint.getUserByLogin(session, "user01");
        Assert.assertNotNull(user);
        Assert.assertEquals(Role.USER, user.getRole());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeGetUserByIdWithoutSession() throws DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.getUserById(null, "user01");
    }

    @Test(expected = EmptyIdException_Exception.class)
    public void testNegativeGetUserByIdWithoutId() throws DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.getUserById(session, null);
    }

    @Test
    public void testGetUserById() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPassword(session, "user01", "password");
        @NotNull UserDTO userByLogin = userEndpoint.getUserByLogin(session, "user01");
        @NotNull UserDTO userById = userEndpoint.getUserById(session, userByLogin.getId());
        Assert.assertEquals(userByLogin.getId(), userById.getId());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeGetUserByLoginWithoutSession() throws EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.getUserByLogin(null, "user01");
    }

    @Test(expected = EmptyLoginException_Exception.class)
    public void testNegativeGetUserByLoginWithoutLogin() throws EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.getUserByLogin(session, null);
    }

    @Test
    public void testGetUserByLogin() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPassword(session, "user01", "password");
        @NotNull UserDTO userByLogin = userEndpoint.getUserByLogin(session, "user01");
        Assert.assertNotNull(userByLogin);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeEditUserProfileByIdFirstNameWithoutSession() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception {
        userEndpoint.editUserProfileByIdFirstName(null, "id", "FirstName");
    }

    @Test(expected = EmptyIdException_Exception.class)
    public void testNegativeEditUserProfileByIdFirstNameWithoutId() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception {
        userEndpoint.editUserProfileByIdFirstName(session, null, "FirstName");
    }

    @Test(expected = EmptyFirstNameException_Exception.class)
    public void testNegativeEditUserProfileByIdFirstNameWithoutFirstName() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception {
        userEndpoint.editUserProfileByIdFirstName(session, "id", null);
    }

    @Test
    public void EditUserProfileByIdFirstName() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPassword(session, "user01", "password");
        @NotNull UserDTO userByLogin = userEndpoint.getUserByLogin(session, "user01");
        Assert.assertNull(userByLogin.getFirstName());

        userEndpoint.editUserProfileByIdFirstName(session, userByLogin.getId(), "FirstName");
        userByLogin = userEndpoint.getUserByLogin(session, "user01");
        Assert.assertEquals("FirstName", userByLogin.getFirstName());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeEditUserProfileByIdFirstNameLastNameWithoutSession() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyLastNameException_Exception {
        userEndpoint.editUserProfileByIdFirstNameLastName(null, "id", "FirstName", "LastName");
    }

    @Test(expected = EmptyIdException_Exception.class)
    public void testNegativeEditUserProfileByIdFirstNameLastNameWithoutId() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyLastNameException_Exception {
        userEndpoint.editUserProfileByIdFirstNameLastName(session, null, "FirstName", "LastName");
    }

    @Test(expected = EmptyFirstNameException_Exception.class)
    public void testNegativeEditUserProfileByIdFirstNameLastNameWithoutFirstName() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyLastNameException_Exception {
        userEndpoint.editUserProfileByIdFirstNameLastName(session, "id", null, "LastName");
    }

    @Test(expected = EmptyLastNameException_Exception.class)
    public void testNegativeEditUserProfileByIdFirstNameLastNameWithoutLastName() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyLastNameException_Exception {
        userEndpoint.editUserProfileByIdFirstNameLastName(session, "id", "FirstName", null);
    }

    @Test
    public void EditUserProfileByIdFirstNameLastName() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyLastNameException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPassword(session, "user01", "password");
        @NotNull UserDTO userByLogin = userEndpoint.getUserByLogin(session, "user01");
        Assert.assertNull(userByLogin.getFirstName());
        Assert.assertNull(userByLogin.getLastName());

        userEndpoint.editUserProfileByIdFirstNameLastName(session, userByLogin.getId(), "FirstName", "LastName");
        userByLogin = userEndpoint.getUserByLogin(session, "user01");
        Assert.assertEquals("FirstName", userByLogin.getFirstName());
        Assert.assertEquals("LastName", userByLogin.getLastName());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeEditUserProfileByIdFirstNameLastNameMiddleNameWithoutSession() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyLastNameException_Exception, EmptyEmailException_Exception, EmptyMiddleNameException_Exception {
        userEndpoint.editUserProfileByIdFirstNameLastNameMiddleName(null, "id","email", "FirstName", "LastName", "MiddleName");
    }

    @Test(expected = EmptyIdException_Exception.class)
    public void testNegativeEditUserProfileByIdFirstNameLastNameMiddleNameWithoutId() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyLastNameException_Exception, EmptyEmailException_Exception, EmptyMiddleNameException_Exception {
        userEndpoint.editUserProfileByIdFirstNameLastNameMiddleName(session, null,"email","FirstName", "LastName", "MiddleName");
    }

    @Test(expected = EmptyEmailException_Exception.class)
    public void testNegativeEditUserProfileByIdFirstNameLastNameMiddleNameWithoutEmail() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyLastNameException_Exception, EmptyEmailException_Exception, EmptyMiddleNameException_Exception {
        userEndpoint.editUserProfileByIdFirstNameLastNameMiddleName(session, "id",null,"FirstName", "LastName", "MiddleName");
    }

    @Test(expected = EmptyFirstNameException_Exception.class)
    public void testNegativeEditUserProfileByIdFirstNameLastNameMiddleNameWithoutFirstName() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyLastNameException_Exception, EmptyEmailException_Exception, EmptyMiddleNameException_Exception {
        userEndpoint.editUserProfileByIdFirstNameLastNameMiddleName(session, "id","email", null, "LastName", "MiddleName");
    }

    @Test(expected = EmptyLastNameException_Exception.class)
    public void testNegativeEditUserProfileByIdFirstNameLastNameMiddleNameWithoutLastName() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyLastNameException_Exception, EmptyEmailException_Exception, EmptyMiddleNameException_Exception {
        userEndpoint.editUserProfileByIdFirstNameLastNameMiddleName(session, "id","email", "FirstName", null, "MiddleName");
    }

    @Test(expected = EmptyMiddleNameException_Exception.class)
    public void testNegativeEditUserProfileByIdFirstNameLastNameMiddleNameWithoutNameMiddleName() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyLastNameException_Exception, EmptyEmailException_Exception, EmptyMiddleNameException_Exception {
        userEndpoint.editUserProfileByIdFirstNameLastNameMiddleName(session, "id","email", "FirstName", "LastName", null);
    }

    @Test
    public void testEditUserProfileByIdFirstNameLastNameMiddleName() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyFirstNameException_Exception, EmptyIdException_Exception, EmptyLastNameException_Exception, EmptyEmailException_Exception, EmptyMiddleNameException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPassword(session, "user01", "password");
        @NotNull UserDTO userByLogin = userEndpoint.getUserByLogin(session, "user01");
        Assert.assertNull(userByLogin.getEmail());
        Assert.assertNull(userByLogin.getFirstName());
        Assert.assertNull(userByLogin.getLastName());
        Assert.assertNull(userByLogin.getMiddleName());

        userEndpoint.editUserProfileByIdFirstNameLastNameMiddleName(session, userByLogin.getId(),"email", "FirstName", "LastName", "MiddleName");
        userByLogin = userEndpoint.getUserByLogin(session, "user01");
        Assert.assertEquals("email", userByLogin.getEmail());
        Assert.assertEquals("FirstName", userByLogin.getFirstName());
        Assert.assertEquals("LastName", userByLogin.getLastName());
        Assert.assertEquals("MiddleName", userByLogin.getMiddleName());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeUpdateUserPasswordByIdWithoutSession() throws EmptyHashLineException_Exception, DeniedAccessException_Exception, EmptyUserException_Exception, EmptyIdException_Exception, EmptyNewPasswordException_Exception, IncorrectHashPasswordException_Exception {
        userEndpoint.updateUserPasswordById(null, "id","newPassword");
    }

    @Test(expected = EmptyIdException_Exception.class)
    public void testNegativeUpdateUserPasswordByIdWithoutId() throws EmptyHashLineException_Exception, DeniedAccessException_Exception, EmptyUserException_Exception, EmptyIdException_Exception, EmptyNewPasswordException_Exception, IncorrectHashPasswordException_Exception {
        userEndpoint.updateUserPasswordById(session, null,"newPassword");
    }

    @Test(expected = EmptyNewPasswordException_Exception.class)
    public void testNegativeUpdateUserPasswordByIdWithoutNewPassword() throws EmptyHashLineException_Exception, DeniedAccessException_Exception, EmptyUserException_Exception, EmptyIdException_Exception, EmptyNewPasswordException_Exception, IncorrectHashPasswordException_Exception {
        userEndpoint.updateUserPasswordById(session, "id",null);
    }

    @Test
    public void testUpdateUserPasswordById() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyIdException_Exception, EmptyNewPasswordException_Exception, IncorrectHashPasswordException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPassword(session, "user01", "password");
        @NotNull UserDTO userByLogin = userEndpoint.getUserByLogin(session, "user01");

        userEndpoint.updateUserPasswordById(session, userByLogin.getId(),"newPassword");
        @NotNull UserDTO updatedUser = userEndpoint.getUserByLogin(session, "user01");
        Assert.assertNotEquals(userByLogin.getPasswordHash(), updatedUser.getPasswordHash());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeRemoveUserByIdWithoutSession() throws DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.removeUserById(null, "id");
    }

    @Test(expected = EmptyIdException_Exception.class)
    public void testNegativeRemoveUserByIdWithoutId() throws DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.removeUserById(session, null);
    }

    @Test
    public void testRemoveUserById() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPassword(session, "user01", "password");
        @NotNull UserDTO userByLogin = userEndpoint.getUserByLogin(session, "user01");

        userEndpoint.removeUserById(session, userByLogin.getId());
        @Nullable UserDTO deletedUser = userEndpoint.getUserById(session, userByLogin.getId());
        Assert.assertNull(deletedUser);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeRemoveUserByLoginWithoutSession() throws DeniedAccessException_Exception, EmptyLoginException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.removeUserByLogin(null, "user01");
    }

    @Test(expected = EmptyLoginException_Exception.class)
    public void testNegativeRemoveUserByLoginWithoutLogin() throws DeniedAccessException_Exception, EmptyLoginException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.removeUserByLogin(session, null);
    }

    @Test
    public void testRemoveUserByLogin() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyIdException_Exception, EmptyNewPasswordException_Exception, IncorrectHashPasswordException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.addUserLoginPassword(session, "user01", "password");
        @NotNull UserDTO userByLogin = userEndpoint.getUserByLogin(session, "user01");
        Assert.assertNotNull(userByLogin);

        userEndpoint.removeUserByLogin(session, "user01");
        userByLogin = userEndpoint.getUserByLogin(session, "user01");
        Assert.assertNull(userByLogin);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeClearAllUserWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, BusyLoginException_Exception, EmptySessionException_Exception, EmptyLoginException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, EmptyPasswordException_Exception, NotExistUserException_Exception, EmptyRoleException_Exception {
        userEndpoint.clearAllUser(null);
    }

    @Test
    public void testClearAllUser() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
        userEndpoint.addUserLoginPassword(session, "user01", "password");
        userEndpoint.addUserLoginPassword(session, "user02", "password");
        userEndpoint.addUserLoginPassword(session, "user03", "password");
        userEndpoint.addUserLoginPassword(session, "user04", "password");
        userEndpoint.addUserLoginPassword(session, "user05", "password");

        userEndpoint.clearAllUser(session);
        session = sessionEndpoint.openSession("admin", "admin");
        @NotNull final List<UserDTO> users = userEndpoint.getUserList(session);
        Assert.assertEquals(3, users.size());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeGetUserListWithoutSession() throws DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        userEndpoint.getUserList(null);
    }

    @Test
    public void testGetUserList() throws EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
        userEndpoint.clearAllUser(session);
        session = sessionEndpoint.openSession("admin", "admin");
        @NotNull List<UserDTO> users = userEndpoint.getUserList(session);
        Assert.assertEquals(3, users.size());

        userEndpoint.addUserLoginPassword(session, "user01", "password");
        userEndpoint.addUserLoginPassword(session, "user02", "password");
        userEndpoint.addUserLoginPassword(session, "user03", "password");
        users = userEndpoint.getUserList(session);
        Assert.assertEquals(6, users.size());
    }

}
