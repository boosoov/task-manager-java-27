package com.rencredit.jschool.boruak.taskmanager.integration.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IEndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.locator.EndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

@Category(IntegrationWithServerTestCategory.class)
public class AdminUserEndpointTest {

    @NotNull IEndpointLocator endpointLocator;
    @NotNull AdminUserEndpoint adminUserEndpoint;
    @NotNull UserEndpoint userEndpoint;
    @NotNull AuthEndpoint authEndpoint;
    @NotNull SessionEndpoint sessionEndpoint;
    @NotNull SessionDTO session;

    @Before
    public void init() throws EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, EmptyHashLineException_Exception, DeniedAccessException_Exception, BusyLoginException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
        endpointLocator = new EndpointLocator();
        adminUserEndpoint = endpointLocator.getAdminUserEndpoint();
        userEndpoint = endpointLocator.getUserEndpoint();
        sessionEndpoint = endpointLocator.getSessionEndpoint();
        authEndpoint = endpointLocator.getAuthEndpoint();

        session = sessionEndpoint.openSession("admin", "admin");
    }

    @After
    public void clearAll() throws DeniedAccessException_Exception {
//        sessionEndpoint.closeSession(session);
    }

    @Test
    public void testRoles() throws DeniedAccessException_Exception {
        List<Role> role = adminUserEndpoint.roles();
        Assert.assertEquals(Role.ADMIN, role.get(0));
        sessionEndpoint.closeSession(session);
    }

    @Test
    public void testLockUnlockUserByLogin() throws NotExistUserException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptySessionException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, EmptyLoginException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception {
        adminUserEndpoint.lockUserByLogin(session,"test");
        @NotNull UserDTO user = userEndpoint.getUserByLogin(session,"test");
        Assert.assertTrue(user.isLocked());

        adminUserEndpoint.unlockUserByLogin(session,"test");
        user = userEndpoint.getUserByLogin(session,"test");
        Assert.assertFalse(user.isLocked());
        sessionEndpoint.closeSession(session);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testLoginLockedUser() throws NotExistUserException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptySessionException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, EmptyLoginException_Exception, EmptyUserException_Exception, EmptyPasswordException_Exception, EmptyHashLineException_Exception, BusyLoginException_Exception {
        adminUserEndpoint.lockUserByLogin(session,"test");
        sessionEndpoint.closeSession(session);
        @NotNull SessionDTO sessionTest = sessionEndpoint.openSession("test", "test");
        Assert.assertNull(sessionTest);
        adminUserEndpoint.unlockUserByLogin(session,"test");
    }

}
