package com.rencredit.jschool.boruak.taskmanager.command.info;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public final class SystemInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String name() {
        return "info";
    }

    @NotNull
    @Override
    public String description() {
        return "Show computer info.";
    }

    @Override
    public void execute() {
        System.out.println("[SYSTEM INFO]");
        System.out.println(serviceLocator.getInfoService().getSystemInfo());
    }

}
