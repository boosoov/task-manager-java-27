package com.rencredit.jschool.boruak.taskmanager.command.info;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import org.jetbrains.annotations.NotNull;

public class ServerInfoShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "server-info";
    }

    @NotNull
    @Override
    public String description() {
        return "Show show host and port program.";
    }

    @Override
    public void execute() throws NotExistUserException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptySessionException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        System.out.println("[SERVER INFO]");
        @NotNull final SessionDTO session = serviceLocator.getSystemObjectService().getSession();
        System.out.println("HOST:" + endpointLocator.getAdminEndpoint().getHost(session));
        System.out.println("PORT:" + endpointLocator.getAdminEndpoint().getPort(session));
        System.out.println("OK");
    }

}
