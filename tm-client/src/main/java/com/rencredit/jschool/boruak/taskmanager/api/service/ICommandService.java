package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyCommandException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyNameException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public interface ICommandService {

    @NotNull
    Map<String, AbstractCommand> getTerminalCommands();

    @NotNull
    String[] getCommands();

    @NotNull
    String[] getArgs();

    void putCommand(@Nullable final String name, @Nullable final AbstractCommand command) throws EmptyCommandException, EmptyNameException;

}
