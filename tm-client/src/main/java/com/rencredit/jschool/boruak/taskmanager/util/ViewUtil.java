package com.rencredit.jschool.boruak.taskmanager.util;

import com.rencredit.jschool.boruak.taskmanager.endpoint.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.endpoint.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.endpoint.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import org.jetbrains.annotations.Nullable;

public class ViewUtil {

    public static void showUser(@Nullable final UserDTO user) throws EmptyUserException {
        if (user == null) throw new EmptyUserException();
        System.out.println("Email " + user.getEmail());
        System.out.println("Login " + user.getLogin());
        System.out.println("Role " + user.getRole());
        System.out.println("Password Hash " + user.getPasswordHash());
        System.out.println("First Name " + user.getFirstName());
        System.out.println("Middle Name " + user.getMiddleName());
        System.out.println("Last Name " + user.getLastName());
    }

    public static void showTask(@Nullable final TaskDTO task) throws EmptyUserException {
        if (task == null) throw new EmptyUserException();
        System.out.println("Id " + task.getId());
        System.out.println("Name " + task.getName());
        System.out.println("Description " + task.getDescription());
        System.out.println("User Id " + task.getUserId());
    }

    public static void showProject(@Nullable final ProjectDTO project) throws EmptyUserException {
        if (project == null) throw new EmptyUserException();
        System.out.println("Id " + project.getId());
        System.out.println("Name " + project.getName());
        System.out.println("Description " + project.getDescription());
        System.out.println("User Id " + project.getUserId());
    }

}
