package com.rencredit.jschool.boruak.taskmanager.command;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IEndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyProjectException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptySessionException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.util.Objects;

public abstract class AbstractCommand {

    @NotNull
    protected IEndpointLocator endpointLocator;

    @NotNull
    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setEndpointLocator(
            @NotNull IEndpointLocator endpointLocator
    ) {
        this.endpointLocator = endpointLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String name();

    @NotNull
    public abstract String description();

    public abstract void execute() throws IOException, ClassNotFoundException, IOException_Exception, ClassNotFoundException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyNewPasswordException_Exception, IncorrectHashPasswordException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyRoleException_Exception, EmptySessionException_Exception, EmptyUserIdException_Exception, EmptyLoginException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyLastNameException_Exception, EmptyEmailException_Exception, EmptyMiddleNameException_Exception, EmptyFirstNameException_Exception, EmptyTaskException_Exception, IncorrectIndexException_Exception, EmptyNameException_Exception, EmptyDescriptionException_Exception, EmptyProjectException_Exception, EmptyElementsException_Exception, EmptyCommandException, EmptyUserException, EmptyProjectException, EmptyTaskException, EmptySessionException, EmptyProjectIdException_Exception, EmptyTaskIdException_Exception;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractCommand command = (AbstractCommand) o;
        return endpointLocator.equals(command.endpointLocator) &&
                serviceLocator.equals(command.serviceLocator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(endpointLocator, serviceLocator);
    }

    @Override
    public String toString() {
        return "AbstractCommand{" +
                "endpointLocator=" + endpointLocator +
                ", serviceLocator=" + serviceLocator +
                '}';
    }

}
