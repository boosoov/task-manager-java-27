package com.rencredit.jschool.boruak.taskmanager.util;

import com.rencredit.jschool.boruak.taskmanager.constant.ArgumentConst;
import com.rencredit.jschool.boruak.taskmanager.constant.TerminalConst;
import org.jetbrains.annotations.Nullable;

public class TerminalCommandUtil {

    public TerminalCommandUtil() {
    }

    @Nullable
    public static String convertArgumentToCommand(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return arg;
        switch (arg) {
            case ArgumentConst.HELP:
                return TerminalConst.HELP;
            case ArgumentConst.ABOUT:
                return TerminalConst.ABOUT;
            case ArgumentConst.VERSION:
                return TerminalConst.VERSION;
            case ArgumentConst.INFO:
                return TerminalConst.INFO;
            case ArgumentConst.EXIT:
                return TerminalConst.EXIT;
            case ArgumentConst.ARGUMENTS:
                return TerminalConst.ARGUMENTS;
            case ArgumentConst.COMMANDS:
                return TerminalConst.COMMANDS;
            default:
                return arg;
        }
    }

}
