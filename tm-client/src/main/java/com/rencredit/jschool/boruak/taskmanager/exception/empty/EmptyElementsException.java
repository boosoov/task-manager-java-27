package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyElementsException extends AbstractClientException {

    public EmptyElementsException() {
        super("Error! Elements is empty...");
    }

}
