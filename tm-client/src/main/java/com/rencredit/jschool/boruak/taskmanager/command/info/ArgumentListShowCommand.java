package com.rencredit.jschool.boruak.taskmanager.command.info;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public class ArgumentListShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        System.out.println(serviceLocator.getInfoService().getArgumentList());
    }

}
