package com.rencredit.jschool.boruak.taskmanager.api.locator;

import com.rencredit.jschool.boruak.taskmanager.api.service.ICommandService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IInfoService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ISystemObjectService;
import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IInfoService getInfoService();

    @NotNull
    ISystemObjectService getSystemObjectService();
    
}
