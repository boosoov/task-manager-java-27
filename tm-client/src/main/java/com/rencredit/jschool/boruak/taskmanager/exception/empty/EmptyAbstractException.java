package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyAbstractException extends AbstractClientException {

    public EmptyAbstractException() {
        super("Error! Abstract is empty...");
    }

}
